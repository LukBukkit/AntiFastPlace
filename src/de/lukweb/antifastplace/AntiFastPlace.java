package de.lukweb.antifastplace;
/*
The MIT License (MIT)

Copyright (c) 2015 LukBukkit (lukweb.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.lukweb.antifastplace.commands.InfoCMD;
import de.lukweb.antifastplace.events.BlockPlaceEV;
import de.lukweb.antifastplace.events.PlayerDisconnectEV;

public class AntiFastPlace extends JavaPlugin {
	
	private static AntiFastPlace instance;

	@Override
	public void onDisable() {

	}

	@Override
	public void onEnable() {
		instance = this;
		saveDefaultConfig();
		this.getCommand("antifastplace").setExecutor(new InfoCMD());
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new BlockPlaceEV(), this);
		pm.registerEvents(new PlayerDisconnectEV(), this);
	}
	
	public static AntiFastPlace getInstance(){
		return instance;
	}

}
