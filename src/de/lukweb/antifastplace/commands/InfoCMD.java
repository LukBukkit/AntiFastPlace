package de.lukweb.antifastplace.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class InfoCMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		String[] msg = new String[]{
				"",
				ChatColor.GREEN + "" + ChatColor.BOLD + "AntiFastBuild " + ChatColor.RESET + "" + ChatColor.BLUE + "by LukBukkit ( http://lukweb.de )",
				""
		};
		cs.sendMessage(msg);
		return true;
	}

}
