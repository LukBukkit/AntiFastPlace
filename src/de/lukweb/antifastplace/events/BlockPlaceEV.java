package de.lukweb.antifastplace.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import de.lukweb.antifastplace.AntiFastPlace;

public class BlockPlaceEV implements Listener {

	private static HashMap<String, Long> lastblock = new HashMap<>();
	private static HashMap<String, BlocksOverTime> bovertime = new HashMap<>();

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		if (e.getPlayer().hasPermission("antifastplace.skip")) {
			return;
		}
		boolean failed = false;
		if (lastblock.containsKey(e.getPlayer().getName())) {
			if (lastblock.get(e.getPlayer().getName()) + AntiFastPlace.getInstance().getConfig().getInt("delay-between-blockplaces") >= System.currentTimeMillis()) {
				e.setBuild(false);
				e.setCancelled(true);
				failed = true;
			}
		}
		lastblock.put(e.getPlayer().getName(), System.currentTimeMillis());

		if (!bovertime.containsKey(e.getPlayer().getName())) {
			bovertime.put(e.getPlayer().getName(), new BlocksOverTime(e.getPlayer().getName()));
		}
		bovertime.get(e.getPlayer().getName()).add();
		if (bovertime.get(e.getPlayer().getName()).getOverTime() > AntiFastPlace.getInstance().getConfig().getInt("max-blocks-in-5sec")) {
			e.setBuild(false);
			e.setCancelled(true);
			failed = true;
		}
		if(failed){
			sendFailMessage(e.getPlayer());
		}
	}

	private void sendFailMessage(Player failer) {
		if (AntiFastPlace.getInstance().getConfig().getBoolean("send-message-on-fail")) {
			failer.sendMessage(AntiFastPlace.getInstance().getConfig().getString("message-to-player"));
		}
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.hasPermission("antifastplace.admin")) {
				p.sendMessage(AntiFastPlace.getInstance().getConfig().getString("message-to-team"));
			}
		}

	}

	public static void clearLists(String name) {
		lastblock.remove(name);
		bovertime.remove(name);
	}

	public static class BlocksOverTime {

		private final String name;
		private ArrayList<Long> placed = new ArrayList<>();

		public BlocksOverTime(String name) {
			this.name = name;
		}

		public void add() {
			placed.add(System.currentTimeMillis());
		}

		public int getOverTime() {
			for (Iterator<Long> it = placed.iterator(); it.hasNext();) {
				Long l = it.next();
				if ((l + 5000) >= System.currentTimeMillis()) {
					// MAYBE ADD SOMETHING HERE :P
				} else {
					it.remove();
				}
			}
			return placed.size();
		}

		public String getName() {
			return name;
		}

	}

}
