package de.lukweb.antifastplace.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerDisconnectEV implements Listener{
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent e){
		BlockPlaceEV.clearLists(e.getPlayer().getName());
	}

}
